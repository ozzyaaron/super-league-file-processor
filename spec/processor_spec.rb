require "spec_helper"

describe "processing" do
  # Good to have one integration test for calling the shell script and then
  # deeper tests of the classes separately.
  #
  # Not all calls should use system, this just tests that inputs flow correctly
  # to other unit tests classes. This is an integration test of sorts.
  #
  let(:example_output_file_content) { File.read("expected-output.txt") + "\n" }

  it "can process the sample input file as an arg" do
    expect { system("./processor.rb sample-input.txt") }.
      to output(example_output_file_content).to_stdout_from_any_process

    expect($?.exitstatus).to eq(0)
  end

  it "can process the sample input file as piped input" do
    expect { system("cat sample-input.txt | ./processor.rb") }.
      to output(example_output_file_content).to_stdout_from_any_process

    expect($?.exitstatus).to eq(0)
  end

  it "can process the sample input file as redirected input" do
    expect { system("./processor.rb < sample-input.txt") }.
      to output(example_output_file_content).to_stdout_from_any_process

    expect($?.exitstatus).to eq(0)
  end

  it "fails for a broken first match day" do
    expect { system("./processor.rb file-with-broken-first-match-day.txt") }.
      to output("An error was found in the First Match Day\n").
      to_stderr_from_any_process

    expect($?.exitstatus).to eq(1)
  end

  it "outputs broken match days correctly" do
    example_output_file_content = File.read("expected-output-for-file-with-broken-match-day.txt") + "\n"

    expect { system("./processor.rb file-with-broken-match-day.txt") }.
      to output(example_output_file_content).to_stdout_from_any_process.
      to_stdout_from_any_process

    expect($?.exitstatus).to eq(0)
  end

  it "fails for other types of calls" do
    expect { system("./processor.rb sample-input.txt other_args") }.
      to output("Too many arguments. I'd have a better error message if this were real life!\n").
      to_stderr_from_any_process

    expect($?.exitstatus).to eq(1)
  end
end
