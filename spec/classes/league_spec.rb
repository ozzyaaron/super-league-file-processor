require "spec_helper"
require_relative "../../classes/league.rb"

describe League do
  let(:league) { described_class.new }

  describe "#current_match_day_finished?" do
    subject { league.current_match_day_finished? }

    before { allow(league).to receive(:current_match_day).and_return(double(finished?: match_day_finished)) }

    context "current_match_day.finished?" do
      let(:match_day_finished) { true }

      it { is_expected.to be(true) }
    end

    context "!current_match_day.finished?" do
      let(:match_day_finished) { false }

      it { is_expected.to be(false) }
    end
  end

  # Some operations are more the place of integration tests.
  #
  # For instance replacing @match_day, this is internal state
  # and if it weren't replaced the output would be wrong.
  #
  describe "#current_match_day_finished_tasks" do
    subject { league.current_match_day_finished_tasks }

    before { allow(league).to receive(:current_match_day_finished?).and_return(current_match_day_finished) }

    context "current_match_day_finished?" do
      let(:current_match_day_finished) { true }

      before do
        allow(league).to receive(:current_match_day).
          and_return(
            double(
              valid?: match_day_valid,
              invalid?: !match_day_valid,
              league_scores: {
                a: 5,
                b: 6,
              }
            ),
          )
      end

      context "current_match_day.valid?" do
        let(:match_day_valid) { true }

        it "prints the match day scores and increments match_count" do
          expect { subject }.
            to output("Matchday 1\nb, 6 pts\na, 5 pts\n\n").
            to_stdout.
            and change { league.match_count }.by(1)
        end
      end

      context "!current_match_day.valid?" do
        let(:match_day_valid) { false }

        it "prints the match day scores with a decoration and increments match count" do
          expect { subject }.
            to output("Matchday 1 ** BROKEN **\n\n\n").
            to_stdout.
            and change { league.match_count }.by(1)
        end
      end
    end

    context "!current_match_day_finished?" do
      let(:current_match_day_finished) { false }

      it "doesn't increment match_count" do
        expect { subject }.not_to change { league.match_count }
      end

      it "doesn't print anything" do
        expect { subject }.to output("").to_stdout
      end
    end
  end
end
