require "spec_helper"
require_relative "../../classes/game"

describe Game do
  let(:game) { described_class.new(line) }

  describe "#valid?" do
    subject { game.valid? }

    context "line with <string> <int>, <string> <int>" do
      let(:line) { "team a 3, team b 3" }

      it { is_expected.to be(true) }
    end

    context "line with without points" do
      let(:line) { "team a , team b" }

      it { is_expected.to be(false) }
    end

    context "team names are the same" do
      let(:line) { "team a 3, team a 3" }

      it { is_expected.to be(false) }
    end

    context "points are not integers" do
      let(:line) { "team a 3.2, team b 3.a" }

      it { is_expected.to be(false) }
    end

    context "there are not 2 teams and 2 scores" do
      let(:line) { "team a 3 team b 3" }

      it { is_expected.to be(false) }
    end

    context "there is a line with a comma and no trailing values" do
      let(:line) { "team a 3," }

      it { is_expected.to be(false) }
    end

    context "line is empty" do
      let(:line) { "" }

      it { is_expected.to be(false) }
    end

    context "line is nil" do
      let(:line) { nil }

      it { is_expected.to be(false) }
    end
  end

  describe "#teams_with_league_points" do
    subject { game.teams_with_league_points }

    context "a draw" do
      let(:line) { "team a 3, team b 3" }

      it { is_expected.to eq("team a" => 1, "team b" => 1) }
    end

    context "team a wins" do
      let(:line) { "team a 3, team b 2" }

      it { is_expected.to eq("team a" => 3, "team b" => 0) }
    end

    context "team b wins" do
      let(:line) { "team a 3, team b 5" }

      it { is_expected.to eq("team a" => 0, "team b" => 3) }
    end
  end
end
