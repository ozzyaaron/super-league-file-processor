require "spec_helper"
require_relative "../../classes/regular_match_day"

describe RegularMatchDay do
  let(:match_day) { described_class.new(first_match_day: first_match_day, match_count: match_count) }
  let(:first_match_day) { double(expected_game_count: 2, next_match_day_game: double) }
  let(:match_count) { 2 }

  describe "#finished?" do
    subject { match_day.finished? }

    context "games.length < expected_game_count" do
      it { is_expected.to be(false) }
    end

    context "games.length == expected_game_count" do
      before { match_day.add_game("a game") }

      it { is_expected.to be(true) }
    end
  end

  describe "#add_game" do
    subject { match_day.add_game("new game") }

    context "games.length < expected_game_count" do
      it "adds the game" do
        expect { subject }.to change { match_day.games.length }.by(1)
      end
    end

    context "games.length == expected_game_count" do
      before { match_day.add_game("new_game") }

      it "adds the game" do
        expect { subject }.to change { match_day.games.length }.by(1)
      end
    end

    context "games.length > expected_game_count" do
      before do
        match_day.add_game("new_game")
        match_day.add_game("new_game")
      end

      it "raises exception" do
        expect { subject }.to raise_error(TooManyGamesError)
      end
    end
  end
end
