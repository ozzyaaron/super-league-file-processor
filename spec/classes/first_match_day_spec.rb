require "spec_helper"
require_relative "../../classes/game"
require_relative "../../classes/first_match_day"

describe FirstMatchDay do
  let(:match_day) { described_class.new }

  describe "#expected_game_count" do
    subject { match_day.expected_game_count }

    context "no games added" do
      it { is_expected.to eq(0) }
    end

    context "some games added" do
      before { match_day.add_game(double(invalid?: false)) }

      it { is_expected.to eq(0) }
    end
  end

  describe "#finished?" do
    subject { match_day.finished? }

    context "we've got no games" do
      it { is_expected.to be(false) }
    end

    context "we've got games with unique team names" do
      before do
        match_day.add_game(Game.new("Team a 1, Team b 2"))
        match_day.add_game(Game.new("Team c 1, Team d 2"))
      end

      it { is_expected.to be(false) }
    end

    context "we've got games with duplicate team names" do
      before do
        match_day.add_game(Game.new("Team a 1, Team b 2"))
        match_day.add_game(Game.new("Team c 1, Team d 2"))
        match_day.add_game(Game.new("Team b 1, Team d 2"))
      end

      it { is_expected.to be(true) }
    end
  end

  describe "#next_match_day_game" do
    subject { match_day.next_match_day_game }

    before do
      match_day.add_game(Game.new("Team a 1, Team b 2"))
      match_day.add_game(last_game)
    end
    let(:last_game) { Game.new("Team b 3, Team c 4") }

    context "#finished?" do
      before { allow(match_day).to receive(:finished?).and_return(true) }

      it { is_expected.to eq(last_game) }
    end

    context "not #finished?" do
      before { allow(match_day).to receive(:finished?).and_return(false) }

      it { is_expected.to be_nil }
    end
  end

  describe "#add_game" do
    subject { match_day.add_game(double(invalid?: game_invalid)) }

    context "not game#invalid?" do
      let(:game_invalid) { false }

      it "adds the game" do
        expect { subject }.to change { match_day.games.length }.by(1)
      end
    end

    context "game#invalid?" do
      let(:game_invalid) { true }

      it "adds the game" do
        expect { subject }.to raise_error(FirstMatchDayIsBroken)
        expect(match_day.games.length).to eq(0)
      end
    end
  end

  describe "#games_counting_to_league_scores" do
    subject { match_day.games_counting_to_league_scores }

    before do
      match_day.add_game(first_game)
      match_day.add_game(second_game)
      match_day.add_game(last_game)
    end
    let(:first_game) { double(invalid?: false) }
    let(:second_game) { double(invalid?: false) }
    let(:last_game) { double(invalid?: false) }

    context "#finished?" do
      before { allow(match_day).to receive(:finished?).and_return(true) }

      it { is_expected.to eq([first_game, second_game]) }
    end

    context "not #finished?" do
      before { allow(match_day).to receive(:finished?).and_return(false) }

      it { is_expected.to eq([]) }
    end
  end
end
