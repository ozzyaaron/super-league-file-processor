require "spec_helper"
require_relative "../../classes/game"
require_relative "../../classes/match_day"

describe MatchDay do
  let(:match_day) { described_class.new }

  describe "#league_scores" do
    subject { match_day.league_scores }

    before do
      allow(match_day).to receive(:games_counting_to_league_scores).
        and_return(games_counting_to_league_scores)
    end

    context "#games_counting_to_league_scores.empty?" do
      let(:games_counting_to_league_scores) { [] }

      it { is_expected.to eq({}) }
    end

    context "not #games_counting_to_league_scores.empty?" do
      context "no duplication of team names" do
        let(:games_counting_to_league_scores) do
          [
            double(teams_with_league_points: { a: 2 }),
            double(teams_with_league_points: { b: 3 }),
            double(teams_with_league_points: { c: 5 }),
          ]
        end

        it { is_expected.to eq(a: 2, b: 3, c: 5) }
      end

      context "duplication of team names" do
        let(:games_counting_to_league_scores) do
          [
            double(teams_with_league_points: { a: 2 }),
            double(teams_with_league_points: { b: 3 }),
            double(teams_with_league_points: { a: 5 }),
          ]
        end

        it { is_expected.to eq(a: 5, b: 3) }
      end
    end
  end
end
