echo 'This script assumes you''re running an environment with a Ruby 3.x installed'
echo 'Otherwise please take steps to install rbenv first'
echo 'This script was tested inside "docker run -dit ruby:3.1.2-slim"'

#echo 'Installing rbenv'
#curl -fsSL https://github.com/rbenv/rbenv-installer/raw/HEAD/bin/rbenv-installer | bash
#eval "$(~/.rbenv/bin/rbenv init - bash)"

#echo 'Installing a Ruby'

#rbenv install

echo 'Bundling gems'

bundle --without os_x

echo 'You should now be setup to run the processing.rb script ...'
