require_relative "match_day"

class RegularMatchDay < MatchDay
  def initialize(first_match_day:, match_count:)
    super()

    @first_match_day = first_match_day
    @match_count = match_count

    # I don't like this but I think I need another set of eyes to work out a nicer way
    #
    if @match_count == 2
      add_game(@first_match_day.next_match_day_game)
    end
  end

  def add_game(game)
    if @games.length > expected_game_count
      raise TooManyGamesError.new
    end

    @games.push(game)
  end

  def valid?
    @games.all?(&:valid?)
  end

  def finished?
    @games.length >= expected_game_count
  end

  def games_counting_to_league_scores
    return [] unless finished?

    @games
  end

  private def expected_game_count
    @first_match_day.expected_game_count
  end
end
