# This class intends to
#   - Track the stats for a League
#     - We should know the number of teams but that isn't output anywhere
#     - We should know the team names though again this is not really necessary to generate the result
#     - We could keep a match count here but it is only used for output and we can assume finalising
#         a MatchDay should mean we're moving on to MatchDay N+1
#     - League is used to track the team stats so their points can be aggregated from Game to Game
#
# We could keep every MatchDay we find but that would take up space linearly with League length
# and size. Instead we re-use one MatchDay object over and over and keep the stats as League state.
#
# We do keep around the FirstMatchDay data in an object but that could also be GCed later if required.
#
# So far given the requirements I haven't seen anything that would require keeping MatchDay records around.
#
class League
  attr_reader :match_count

  def initialize
    @match_count = 1
    @teams_with_league_points = {}
    @first_match_day = @match_day = FirstMatchDay.new
  end

  def current_match_day
    @match_day
  end

  def current_match_day_finished?
    current_match_day.finished?
  end

  def current_match_day_finished_tasks
    return unless current_match_day_finished?

    update_league_scores if current_match_day.valid?
    print_match_day_output(broken: current_match_day.invalid?)
    @match_count += 1

    @match_day = RegularMatchDay.new(
      first_match_day: @first_match_day,
      match_count: @match_count,
    )
  end

  private def update_league_scores
    current_match_day.league_scores.each do |team_name, points|
      @teams_with_league_points[team_name] ||= 0
      @teams_with_league_points[team_name] += points
    end
  end

  private def print_match_day_output(broken: false)
    puts <<~TXT
    Matchday #{ @match_day.match_count }#{ ' ** BROKEN **' if broken }
    #{ team_ranking_output }

    TXT
  end

  private def team_ranking_output
    ordered_teams_with_league_points.first(3).map do |team_name, score|
      "#{ team_name }, #{ score } #{ (score.zero? || score > 1) ? 'pts' : 'pt' }"
    end.join("\n")
  end

  private def ordered_teams_with_league_points
    @teams_with_league_points.sort_by do |team_name, score|
      [-score, team_name]
    end
  end

  # Not used, exercise said we should know the teams
  def teams
    @teams_with_league_points.keys.sort
  end

  # Not used, exercise said we should know the team count
  def team_count
    teams.length
  end
end
