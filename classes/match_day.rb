# Make abstract class?
#
class MatchDay
  attr_reader :games, :match_count

  def initialize
    @expected_game_count = nil
    @games = []
    @teams_with_scores = {}
  end

  class ::TooManyGamesError < StandardError; end

  def add_game_line(line)
    add_game(Game.new(line))
  end

  # We never expect a MatchDay to have games that contain duplicate teams
  # to get scores from so merging into the has is fine.
  #
  def league_scores
    games_counting_to_league_scores.reduce({}) do |scores, game|
      scores.merge!(game.teams_with_league_points)
    end
  end

  def invalid?
    !valid?
  end

  # Typically I like to have a global RequiresImplementationError
  # rather than leaning on NotImplementedError
  #
  def games_counting_to_league_scores
    raise "Requires Implementation"
  end
end
