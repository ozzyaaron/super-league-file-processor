# First MatchDay is important in real life and in this example.
#
# It sets the frame for the rest of the League in real life,
# in this example it sets the structure of the following DataFrame/MatchDays.
#
# In real life adding/removing teams mid-league is quite an operation and
# that is reflected here.
#
# We only know a FirstMatchDay is done when we see the first Game that should be in
# the next MatchDay which is a RegularMatchDay.
#
# After the first match day we know how many Games there should be in a RegularMatchDay
# which is useful for input streams of unknown length where we may need to print the output
# of a match day and then wait until enough data comes in to complete another match day.
#
require_relative "match_day"

class FirstMatchDay < MatchDay
  class ::FirstMatchDayIsBroken < StandardError; end

  def initialize
    super
    @match_count = 1
  end

  def add_game(game)
    raise FirstMatchDayIsBroken if game.invalid?

    @games.push(game)
  end

  def valid?
    true
  end

  def finished?
    @games.flat_map(&:teams).uniq.length != @games.flat_map(&:teams).length
  end

  def expected_game_count
    return 0 if @games.empty?

    @games.length - 1
  end

  def games_counting_to_league_scores
    return [] unless finished?

    @games[0..-2]
  end

  def next_match_day_game
    return unless finished?

    @games.last
  end
end
