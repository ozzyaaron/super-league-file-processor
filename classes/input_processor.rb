class InputProcessor
  def initialize(input)
    @input = input
    @league = League.new
  end

  def start_processing
    @input.each do |line|
      next if line.strip!.empty?

      @league.current_match_day.add_game_line(line)

      if @league.current_match_day_finished?
        @league.current_match_day_finished_tasks
      end
    rescue FirstMatchDayIsBroken
      # It may be recoverable depending on what is known about the data but here
      # due to exercise parameters I decided that the First Match day is too
      # important and recovering from it would take too much work.
      #
      abort("An error was found in the First Match Day")
    rescue StandardError => error
      # It may be more appropriate to rescue IOError separately in some cases.
      #
      # Ctrl+C is an Interrupt exception but this script will respond to it as per usual.
      #
      # Here we could report the error in some way to some system or log file.
      #
      # Instead the goal is to consider the MatchDay broken and therefore treat it as finished.
      #
      # TODO: Add a debug mode?
      #
      # TODO: It can sometimes be useful to try to marshal existing data for a reload later or
      #         at least for debugging purposes.

      @league.current_match_day_finished_tasks
    end
  end
end
