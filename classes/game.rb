class Game
  def initialize(line)
    @line = line
  end

  def valid?
    return false if @line.nil?
    return false if @line.empty?

    @line.match?(/\A.* \d+,.* \d+\z/) && input_parsed_to_hash.keys.uniq.length == 2
  end

  def invalid?
    !valid?
  end

  # League Point logic is
  #   - Win 3 pts
  #   - Loss 0 pts
  #   - Draw 1 pts
  #
  def teams_with_league_points
    @teams_with_scores ||=
      # Ruby's Hash is ordered, the docs confirm Hash#values are ordered
      #
      if input_parsed_to_hash.values.uniq.length == 1
        set_league_scores_for_draw
      elsif input_parsed_to_hash.values.first > input_parsed_to_hash.values.last
        set_league_scores_for_first_team_wins
      else
        set_league_scores_for_last_team_wins
      end
  end

  private def input_parsed_to_hash
    @input_parsed_to_hash ||=
      @line.split(",").map(&:strip).map do |name_with_score|
        match_data = name_with_score.match(/\A(.*) (\d+)\z/)

        [match_data[1], match_data[2].to_i]
      end.to_h
  end

  private def set_league_scores_for_draw
    input_parsed_to_hash.transform_values { 1 }
  end

  private def set_league_scores_for_first_team_wins
    input_parsed_to_hash.transform_values.with_index { |value, index| index.zero? ? 3 : 0 }
  end

  private def set_league_scores_for_last_team_wins
    input_parsed_to_hash.transform_values.with_index { |value, index| index.zero? ? 0 : 3 }
  end

  def teams
    input_parsed_to_hash.keys
  end
end
