# Things to pay attention to ...

process a file if first argument provided
process stdin if no first argument provided
process a redirect if no first argument provided

need to recognize a match day without any delimiter
  - probably just detect a repeated team name as end of a matchday

if a matchday is not finished then consider it ended
  - causes are probably IO issue or invalid line

output should be to stdout
output should be of top 3 teams in score then alphabetical order
output per match day is at most 3 teams

invalid lines are skipped
  - validity isn't defined, left up to me!

# Assumptions

data streamed is for a league
  - a new call to script with new data per league
  - no competitor list changes during league
there is no error in the first matchday
  - an error in the first matchday can have various impacts
  - trying to remedy it based on later data would make earlier matchdays problematic
the document says invalid lines are skipped
  - I'm going to assume lines are an attempt at recording a game so instead of just
      skipping like a ruby `next` call I'll skip their impact

# Things to maybe do, or in a production use case might be useful ...

- make bigger test scripts using a generator
- make known bad input test
- make a script to generate data for redirection/stdin testing
- benchmark for different solutions? memory usage, speed
- probably have debug mode to output things like incomplete matchday
    - when team count is found, ...
    - good to use when using larger files and scripted outputs from above points
- inline gemfile, bundler/inline

# Questions (these are left to me to make assumptions about or explain)

- if there are more than 3 teams to show output for at the end of match day what happens?
    - I just cut it at 3, no additional output to show there were others that might be shown based on score
- if there is an error in the first matchday it is hard to detect other match days
- a line with an invalid team name can effect matchday detection in first matchday
  - this is just a special case of Broken First Match Day
- what does "streaming data is interrupted" mean?
  - I'm assuming things that can be caught in IOError which will also be caught in StandardError
- what is an invalid line?
  - I'm just assuming it is a line trying to register a game but isn't in the right format
  - In the real world it could be any number of reasons and we also might say that these types of errors should stop all processing
