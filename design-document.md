# Design Document For League Processing Script

Please also see the content in the notes documentation which is more of a stream of thought
whilst reading the PROMPT.md file and various notes.

## The Problem

- A stream of potentially unknown length contains a set of match days in a league.
- Nothing is known at the start about the teams involved in a league.
- We need to discover automatically when match days have finished and then keep track of the league scores of teams
  - There is no match day delimiter, no team information is known
- The league score is a 3 for a win, 0 for a loss and 1 for a draw
- Invalid lines should be skipped
  - Validity left up to the developer
- An unfinished match day should be considered finished
  - Scores of this match day will not count to the league scores
- Output at the end of each match day will include the top 3 teams by points with alpha-sorting as a tie break
  - Output is at most 3 teams

## Initial Thoughts

- The first match day is the most important and sets the protocol for the rest of the match days
- We can discover a first match day when we see the first duplicated team name
- An error in the first match day would be very bad, it may be irrecoverable depending on data importance
- The document said invalid lines are skipped but I'm not sure if that means literally a call to next, or if their impact on scores is skipped, as long as I'm consistent I think the decision that makes sense will be found during development
- There may be more than 3 teams even with a alph-sort tie breaker, we're just going to ignore any remaining teams
- Interrupted streaming data is assumed to be some sort of IO issue and captured in code with an exception catch
- The idea that there would be an unlimited stream of data necessitates using block processing where we're popping content off whatever the IO object given is rather than trying to wait for the entire IO to be read, if that's even possible
  - IO#each basically
  - IO#each takes into account line separation across platforms
  - Also you can't just keep objects around, you need to be re-using them and ensuring GC is cleaning things

At this stage I'm thinking we have classes like

- League
- Game
- Match
- CLI or something to redirect input

## The Way I Worked

This isn't usually part of the design document but given the reason for the exercise I felt it was important.

Basically I really like the Sandi Metz "Flocking Rules" way of development using outside-in TDD.

NOTE: If you haven't had a chance to do a course with Sandi, its a real game changer for OOP.

What I did was

- Write super high level tests that mostly can be seen in `processor_spec.rb`. These form my "integration" tests and the "outside" that my test instruments would be using.
- Then move inside to League then Match then Game until I have passing tests at the outside level.
- Then write unit tests for everything that was created whilst moving inside to the interior classes/objects.

At this point I like to create a waypoint/base. I had a working solution, the code just wasn't good and the unit tests may need fleshing out.

At this point the Flocking part comes in as I iterated on the design.

I think an important inflection point during this exercise was discovering that FirstMatchDay and RegularMatchDay are different enough to be their own classes of objects.

Now that I am considering this the end of the exercise I'm not entirely sure that MatchDay should be a base class or a mixin but I feel like I'm getting to the point of diminishing returns with more code editing.

I guess broadly speaking it broke down to 25% of the time to get to the basecamp of solution that passed integration tests and then 75% of the time just playing with different implementations.

The main rule I find from Flocking or Sandi during this time is to keep your tests green. That's why I made the script to run tests on every change. If you make 50 changes between test runs, it can be hard to discover where the breakage was.
There are ways to add changes without impacting tests, even largish ones, and I like working in this way.

That's enough waxing lyrical on that stuff... I just love coding :)

## The Final Design

Overall there is a small `processing.rb` script that determines the input and either sends stdin or file contents to the code that processes the input.

### Classes

#### League

- It keeps track of the league scores of all teams and the current match count.
- At the end of each MatchDay it will print the current scores for the top 3 teams by score and then by alphabetical sorting.
- It tracks the current match day which effectively hides the concern of FirstMatchDay or RegularMatchDay from the input processing. Nice.
- It has the logic around whether to include invalid or unfinished match day results which seems appropriate too.

#### FirstMatchDay

I feel it is important to discuss here why I created a FirstMatchDay and RegularMatchDay.

This is something that I felt came out of some taking-the-dog-for-a-walk type of thinking where I didn't really like how MatchDay had different concerns mostly surrounding the current match count.

Flocking rules at this point indicated to me that because they revolved so much around match count and that both in this example and real-life league the first match day is different to other match days it might be appropriate to separate MatchDay to FirstMatchDay and RegularMatchDay.
After playing around with that separation I liked the outcome and kept it.

The responsibilities of the FirstMatchDay are

- Discovering the teams in a League
- Discovering the expected match count in a RegularMatchDay
- Discovering if it is corrupt/invalid so that the script can exit
  - You cannot add an invalid Game to the FirstMatchDay for this reason

It also has normal responsibilities for a MatchDay like

- Determining when it is finished
- Outputing the league scores for the MatchDay so League can aggregate them

#### RegularMatchDay

Please see FirstMatchDay for some explanation of how this class came to be.

RegularMatchDay knows how many games are expected in a MatchDay based on the FirstMatchDay.

It has normal responsibilities for a MatchDay like

- Determining when it is finished
- Outputing the league scores for the MatchDay so League can aggregate them
- Determining if the RegularMatchDay was valid
  - If it is invalid the scores don't count to the League points

#### Game

This is one of the classes that essentially stayed untouched from the start of work until the end.

I saw this as sort of a primitive -> object pattern.

Game's main responsibility is really to get a line of input and determine if it can make a valid Game object from it. After that it really just has to generate the league score output for a game.

It has the responsibilities

- Parsing a line into team names and scores
- Determining the teams and their league points for the game
- Providing a list of teams to callers
- Being able to answer whether it is valid or not

#### InputProcessor

- It processes the input line by line and passes it to the League's current match day
- It also captures any IO errors or other "unrecoverable" errors and will exit to the terminal being careful to generate the right exit code.

Originally I was calling this class League until such time as it became very apparent too much was happening here.

I think having a League object with at current_match_day concept also helps to hide the FirstMatchDay and RegularMatchDay considerations.

## Final Design vs First Thoughts

I think Game stayed from the start. I'm reasonably happy with this class though I could see in high throughput situations some refactoring of validity would be useful.

The "CLI" type of class really just stayed inside `processing.rb` and I think given the high level tests I'm pretty happy to leave it how it is. If it were to take on more arguments or other concerns I'd likely look to something like dry-cli.

Initially I had League doing input processing and match day tracking as well as score tracking. I left it like this until the last commit where I extracted InputProcessor and League separately.

I'm pretty happy with InputProcessor as it handles the things I think it should handle and is fairly lean.

League I think as a class makes sense to have. It re-uses MatchDay objects like I thought it would and it has its own logic about how to ignore invalid lines that it has passed off to FirstMatchDay and RegularMatchDay. If the MatchDay is not valid then the scores are not counted, part of this logic is whether the Game is valid or not and so we've achieved skipping lines here. League tracks teams and team count because the exercise said this should be tracked but the data isn't used in any output.

FirstMatchDay and RegularMatchDay are concepts/classes the definitely came out of iterating on the design and finding a clear difference between the use cases. I'm fairly happy with both of these and think they do their jobs well and have logic you'd expect. The one area I'm not entirely happy about is the games handoff when transitioning from FirstMatchDay to RegularMatchDay. I'm not a fan of having too much happening in initializers but its a very simple primitive comparison. In high throughput environments we'd probably want to not do that comparison, but also in high throughput environments we probably wouldn't use so many objects either :)

Overally I'd say that at this stage I've crossed the 80/20 threshold and more development seems to not be providing too much improvement in the design in my opinion.

Having re-read the PROMPT.md I think I covered all the concerns. I think the only area I would think is up for debate is about skipping invalid lines. I think if we were to just skip them we'd probably just need to call League#current_match_day_finished_tasks and remove the exit/guard clause on that method within InputProcessor.
