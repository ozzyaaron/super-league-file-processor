#!/usr/bin/env ruby

Dir["./classes/*.rb"].sort.each { require(_1) }

if ARGV.length == 0
  InputProcessor.new(ARGF).start_processing
elsif ARGV.length == 1
  InputProcessor.new(File.new(ARGV.first)).start_processing
else
  abort("Too many arguments. I'd have a better error message if this were real life!")
end
