# Installation Instructions

- My solution runs in standard Ruby and was developed with Ruby 3.1 but could easily be ported to >= 2.7
- I have included bundler config to install gems into the vendor/bundle folder
  - I would have liked to use bunler/inline but I don't like to pollute system gems and bundler/inline doesn't pay attention to bundler path config.

I've tested the setup using a standard Docker Ruby 3.1 image.

`docker run -dit ruby:3.1.2-slim`

The exercise indicated that more would be required when using a compiled language. For my solution using Ruby I thought it would be reasonable to assume a container with required Ruby version. If one were not available I could use a more basic linux distro and create setup commands for that. I think in real life you'd know more about your target and would work with ops to get it deployed properly.

No more setup should be required and you can skip to [Script Usage Instructions](#script-usage-instructions) below.

## Setup For Running Tests

NOTE FOR THE EXERCISE: Originally this script installed rbenv and a Ruby but upon further reading the exercise notes I thought this was overkill.

You can setup to run the tests with the following command:

`setup.sh`

This will

- Install the bundled gems into ./vendor/bundle/ (only useful for tests)

Now you should be able to run the script using the command:

# Script Usage Instructions

`./processor.rb <a file name>` to process the file at `a file name`

or

`./processor.rb` to process content from STDIN e.g. pasting content of a league file

If you have obtained the git repository you should be able to run:

`./processor.rb sample-input.txt`

process.rb can also process redirected and piped input.

`<a command producing output> | ./process.rb`
`./process.rb < <name of a file>`

If you have obtained the git repository you should be able to run:

`cat sample-input.txt | ./processor.rb`

# Running Tests

After running the setup script `./setup.sh` you should be able to run one of two scripts to run tests.

`./run_specs` will run the specs once.

## In Development

If you want to run tests in a more developer friendly way this has been setup for use on OS X. The intent is that upon file changes all tests would be run automatically.

First run:

`bundle install --with os_x`

For other environments there are more robust tools like guard that provide better cross platform operability.

`./run_specs_for_dev` will run the specs whenever you change a file. This is more useful during development.
